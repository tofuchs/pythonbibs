import numpy as np
import matplotlib.pyplot as plt
import warnings

def GewErr(W):
    if len(W)==0:
        return 0.,0
    return sum(W*W),sum(W)



def ErrorHist(X,weights=None,bins=None,DoPlot=False,bins_c=None,ErrorPlotDict={'fmt':'ro'},HistPlotDict=None,NoError=False):
    RetDict = {}
    if len(X) == 0:
        print 'Emtpy X!'
        print 'Returning empty dict!'
        return {}
    if bins is None:
        if HistPlotDict is not None:
            if HistPlotDict.has_key('bins'):
                bins = HistPlotDict['bins']
            else:
                print 'HistPlotDict defined but has also no bins!'
                print 'Returning empty dict!'
                return {}
        else:
            print 'No Bins Given!'
            print 'HistPlotDict also not defined!!'
            print 'Returning empty dict!'
            return {}
    
    #print type(bins)
    if str(type(bins)).find('int')!=-1:
        bins = np.linspace(min(X),max(X),bins)
    bins = np.array(bins)
    
<<<<<<< HEAD
=======
    if HistPlotDict is None:
        HistPlotDict = {}
    
>>>>>>> a89e65a2e055d9fb952ff951150f1245e39d37f7
    HistPlotDict['bins']=bins
    #print X,bins
    DIGI_X=np.digitize(X,bins=bins)-1
    
    if weights is None:
        weights = np.ones(len(X))
    
    Yerr = np.sqrt(
                np.array(
                    map(lambda x: GewErr(weights[DIGI_X==x]),range(len(bins)-1) )
                        ).transpose()[0]
                    )
    RetDict['Yerr'] = Yerr
    
    T=np.histogram(X,bins=bins,weights=weights)
    RetDict['Y'] =  T[0]
    RetDict['Bins'] = T[1]
    
    if DoPlot:
        if bins_c is None:
            #print 'No bin centers defined'
            bins_c = (bins[:-1]+bins[1:])/2.
        if not NoError:
            plt.errorbar(bins_c,RetDict['Y'],yerr=RetDict['Yerr'],**ErrorPlotDict)
        if HistPlotDict is not None:
            plt.hist(bins_c,weights=RetDict['Y'],**HistPlotDict)
    return RetDict

def PlotRatio(histMC,histData,attribute,xlimits = None,RetVals=False):
    binMeans=(histData['Bins'][:-1]+histData['Bins'][1:])/2
    binLeft=histData['Bins'][:-1]
    plt.gcf()
    plt.ylim(-50,50)
    if xlimits is not None:
        plt.xlim(xlimits[0],xlimits[1])
    plt.grid(alpha=0.2)
    plt.xlabel(attribute)
    plt.ylabel('rel. dev. [%]')
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        ratio = 100*(histMC['Y']/histData['Y']-1)
        _yerr = 100*histMC['Y']/histData['Y']*np.sqrt( (histMC['Yerr']/histMC['Y'])**2 + (histData['Yerr']/histData['Y'])**2 )
        plt.errorbar(binMeans,ratio,ecolor='blue',marker='o',ls=' ',markersize=3,yerr=_yerr)
        plt.axhline(0, color='black',ls='--')
        
    if RetVals:
        return [binMeans,ratio,_yerr]


def main():
    X = np.random.random_sample(1000)*10
    W = np.random.random_sample(1000)
    BINS = [0,1,2,3,4,5,6,7,8,9,10]
    HistPlotDict = {'alpha': 0.5,'label': 'HUIII'}
    DDDD = ErrorHist(X,weights=W,bins=BINS,DoPlot=True,fmt='bo',HistPlotDict=HistPlotDict)
    plt.savefig('ErrorHist_testplot.png')

    
if __name__ == "__main__":
    main()
