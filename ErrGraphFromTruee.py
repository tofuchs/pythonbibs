import numpy as np
import matplotlib.pyplot as plt
import ROOT

def GetErrGraphFromTrueeResultFile(_FILE=None,
                                   _PATH="RealDataResults/",
                                   _GRAPH=None,
                                   _BINS=-1,
                                   _KNOTS=-1,
                                   _DEGFREE=-1
                                   ):
    if _FILE is None:
        print 'No File given! No Graph From Result File!'
        print 'Returning empty dict!'
        return {}
    
    if _GRAPH is None:
        if _BINS!=-1 and _KNOTS!=-1 and _DEGFREE!=-1:
            _GRAPH = 'events_result_bins_'+str(_BINS)+'_knots_'+str(_KNOTS)+'_degFree_'+str(_DEGFREE)+'_Graph'
        else:
            print 'Error in Graph building! Graph:',_GRAPH
            print 'Bins,Knots,Degfree:',_BINS,_KNOTS,_DEGFREE
            print 'Returning empty dict!'
            return {}
    
    RF = ROOT.TFile(_FILE)
    ErrGraph = RF.Get(_PATH + _GRAPH)

    try:
        NBINS = ErrGraph.GetN()
    except:
        print 'Graph '+_GRAPH+' does not exist in '+_PATH+'!'
        print 'Returning empty dict!'
        return {}
    
    X = np.zeros(NBINS)
    Y = np.zeros(NBINS)
    Bins = np.zeros(NBINS+1)
    ErrYHigh = np.zeros(NBINS)
    ErrYLow = np.zeros(NBINS)

    for k in range(0,NBINS):
        X[k]   = ErrGraph.GetX()[k]
        Y[k]   = ErrGraph.GetY()[k]
        ErrYHigh[k] = ErrGraph.GetErrorYhigh(k)
        ErrYLow[k] = ErrGraph.GetErrorYlow(k)
        Bins[k] = ErrGraph.GetX()[k] - ErrGraph.GetErrorXlow(k)

    Bins[NBINS]= ErrGraph.GetX()[k] + ErrGraph.GetErrorXhigh(k)
    
    RF.Close()
    return { 'X': X,
             'Y': Y,
             'Bins': Bins,
             'BinWidth': 10**Bins[1:]-10**Bins[:-1],
             'BinWidthNotPow': 10**Bins[1:]-10**Bins[:-1],
             'ErrYLow': ErrYLow,
             'ErrYHigh':ErrYHigh
             }

def CheckDict(Dict={}):
    if not Dict.has_key('fmt'):
        Dict['fmt'] = 'o'
    if not Dict.has_key('capthick'):
        Dict['capthick'] = 2
    return Dict

def PlotTrueeErrorGraph(_DICT={},DivideByBin=False,PlotDict={}):
    plt.gcf()
    PlotDict = CheckDict(PlotDict)
    
    if DivideByBin:
        EB = plt.errorbar(_DICT['X'],_DICT['Y']/_DICT['BinWidth'],yerr=[_DICT['ErrYLow']/_DICT['BinWidth'],_DICT['ErrYLow']/_DICT['BinWidth']],**PlotDict)
    else:
        EB = plt.errorbar(_DICT['X'],_DICT['Y'],yerr=[_DICT['ErrYLow'],_DICT['ErrYLow']],**PlotDict)
        
def main():
    FILEPATH = '/home/tfuchs/Phd/MuonSpektrum/Unfolding/TrueeResultFile.root'
    MC = GetErrGraphFromTrueeResultFile(_FILE=FILEPATH,
                                    _BINS=15,
                                    _KNOTS=17,
                                    _DEGFREE=7)
    PlotTrueeErrorGraph(MC)
    plt.yscale('log')
    plt.show()
    
if __name__ == "__main__":
    main()
