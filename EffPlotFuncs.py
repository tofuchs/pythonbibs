import numpy as np
import sys
import time
import warnings
import logging

GER = {'eff': 'Effizienz','zen':'Zenit','energy':'Log10(E/GeV)'}
ENG = {'eff': 'Efficiency','zen':'zenith','energy':'Log10(E/GeV)'}

from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt

def DictToRecArray(DICT={},SaveArray=None):
    _RecData = []
    _RecDtype = []
    for Name,Data in DICT.items():
        #print Name,Data
        _RecData.append(Data)
        _RecDtype.append((Name,Data.dtype))
    
    A = np.rec.fromarrays(_RecData,dtype=_RecDtype)
    if SaveArray:
        np.save(SaveArray,A)
    
    return A

def PlotEffPerE(EBins=None,
                Data0=None,
                W0 = None,
                DataFilt=True,
                Binned=True,
                xlabel='Log10(E/GeV)',
                ylabel='Efficiency [%]',
                title ='',
                Label='DummyLabel',
                Log=True,
                SaveName='dummy.png',
                SavePath='',
                Draw=True,
                Show=False,
                Color=None):
    EBins_C = (EBins[1:]+EBins[:-1])/2
    DataCut=0
    if not Binned:
        DataCut = np.histogram(Data0[DataFilt],bins=EBins,weights=W0[DataFilt])
        Data0 = np.histogram(Data0,bins=EBins,weights=W0)
    
    WDat = 0
    with np.errstate(invalid='ignore'):
        WDat = DataCut[0]/Data0[0]
        WDat[np.isfinite(WDat)==False] = 0
    h1 = plt.hist(EBins_C,
         weights=WDat*100,
         bins=EBins,
         log=Log,
         histtype='step',
         label=Label,
         linewidth=1.5,
         color=Color)
    plt.ylim(0,140)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if Draw:
        plt.legend(loc=2,ncol=2)
        SaveFig(Name=SavePath+SaveName)
        if Show:
            plt.show()
        else:
            plt.clf()
    return h1

def YmaxInEffPlot(Max=100):
    Div=10

    l10max = int(np.log10(Max))
    Div = Div**(l10max-(np.sign(np.log10(Max))==-1)*1)

    #print Div

    Ymax = Max+Div
    return Ymax - Ymax%Div

EBins = np.arange(3.0,7.0,1./5)
ZenBins = np.linspace(0,np.pi/2,15)
