
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pylab as plt


# In[2]:

def spheric_az_zen(az=0,zen=0,deg=False):
    if deg:
        az = np.deg2rad(az)
        zen = np.deg2rad(zen)
    x = -np.sin(zen)*np.sin(az)
    y = -np.sin(zen)*np.cos(az)
    z = -np.cos(zen)
    return [x,y,z]


# In[44]:

def CylinderOuterPos(X=[0,0,0],Az=0,Zen=0,Cyl_R=1.,Cyl_H=1.):
    if Zen==0 or Zen==180:
        Zen+=1e-3
    Y = np.array(X)
    V = np.array(spheric_az_zen(az=Az,zen=Zen,deg=False))
    alphas = min(np.roots([sum(V[:2]*V[:2]),2*sum(V[:2]*Y[:2]),sum(Y[:2]*Y[:2])-Cyl_R*Cyl_R]))
    return Y+V*max(alphas,(Cyl_H/2 - Y[2]) / V[2])


# In[57]:

def slantDepth(pos=None,Az=0,Zen=0,InDeg=False,detec_depth = 2000,InCylinderOut=False):
    if pos is None:
        pos=[0.,0.,0.]
    vel = np.array(spheric_az_zen(az=Az,zen=Zen,deg=False))
    if InCylinderOut:
        pos = CylinderOuterPos(X=pos,Az=Az,Zen=Zen,Cyl_R=500.,Cyl_H=1000.)
    #print pos
    earthrad = 6371*1e3
    pos[2] = pos[2]+earthrad-detec_depth
    p = [1,0,0]
    p[1] = 2*np.dot(pos,vel)
    p[2] = np.dot(pos,pos)-earthrad*earthrad
    
    sol = np.roots(p)[1]
    if np.iscomplex(sol):
        print sol
        return -1
    return abs(sol)


# In[74]:
def main():
    DegMin=0
    DegMax=80
    Deg = np.linspace(DegMin,DegMax,num=2000)
    for Cog in [500,0,-500]:
        Depth = map(lambda x: slantDepth(Az=0,
                                         Zen=np.deg2rad(x),
                                         pos=[0,0,Cog],
                                         InCylinderOut=True)/1000
                    , Deg )
        plt.plot(Deg,Depth,label=str(Cog)+'m',lw=3)
    #plt.xlim(80,DegMax)
    plt.legend(loc=2)
    plt.xlabel('Zenith')
    plt.ylabel('Depth [km]')
    plt.grid()
    plt.savefig('Depth_vs_Angle.pdf',dpi=300,bbox_inches='tight')
    plt.show()
    

if __name__ == "__main__":
    main()

