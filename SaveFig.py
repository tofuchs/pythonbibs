import matplotlib.pyplot as plt

def SaveFig(Name='DefaultName',Format='both'):
    plt.gcf()
    if Format == 'both' or Format=='pdf':
        plt.savefig(Name+'.pdf',dpi=300,bbox_inches='tight')
    if Format == 'both' or Format=='png':
        plt.savefig(Name+'.png',dpi=300,bbox_inches='tight')
