import numpy as np
import matplotlib.pyplot as plt

def BarNamePlot(Names,Abundance,BarDict={}):
    x = np.array(range(len(Names)))
    y = Abundance
    fig, ax = plt.subplots()
    plt.bar(x,y,**BarDict)

    plt.xticks(np.array(x) + .4,list(Names))


def BarNamePlot_FromNames(Names,BarDict={},Weights=None,NameConverter=None):
    if not Weights:
	Weights = np.ones(len(Names))
    
    TypeSet = list(set(Names))
    print Names==TypeSet[0]
    print TypeSet
    NumType = {i: sum(Weights[Names==i]) for i in TypeSet}
    TypName = []
    TypAb = []
    for nam, ab in NumType.items():
	TypName.append(nam)
	TypAb.append(ab)
    SortType = np.argsort(TypName)
    SortName = np.array(TypName)[SortType]
    SortAb = np.array(TypAb)[SortType]
    
    if NameConverter:
	SortName = NameConverter(SortName)
    
    BarNamePlot(SortName,SortAb)
    
def main():
    X = ['Hund','Katze','Maus']
    Y = [10,2,5]
    
    BarNamePlot(X,Y,{'color':"#7ddc1f",'log':True})
    plt.show()
    
    X = ['Hund','Maus','Maus','Maus','Maus','Maus','Hund','Katze','Maus','Maus','Katze','Hund','Katze','Maus']
    
    BarNamePlot_FromNames(X)
    plt.show()

if __name__ == "__main__":
    main()
