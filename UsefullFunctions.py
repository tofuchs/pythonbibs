import time
import numpy as np
import os

def TimeString(Date=True,Time=True):
    TT = time.localtime()
    TimeStr = ''
    if Date:
        TimeStr = str(TT.tm_year)
        TimeStr += '_'+str(TT.tm_mon)
        TimeStr += '_'+str(TT.tm_mday)
    
    if Time:
        TimeStr += '_'*(len(TimeStr)!=0)+str(TT.tm_hour)
        TimeStr += '_'+str(TT.tm_min)
    
    return TimeStr


def HistBinsForConfidence(NBins=100,Shifter=0):
    dx = (1./NBins)/2
    #print dx
    return np.arange(0-dx,1+dx*2,dx*2)+Shifter

def GetChiSquare(Func,X,Y,Yerr):
    Yexp = Func(X)
    #print 'Yexp: ',Yexp
    Diff = Yexp - Y
    #print 'Diff: ',Diff
    SQVals = Diff**2
    #print 'Sqdiff: ',SQVals
    chi2 = SQVals/Yerr**2
    #print 'Sqdiff: ',chi2
    chi2_sum = sum(chi2)
    #for a,b,c,d,e in zip(Yexp,Y,Yerr,chi2,X):
    #    print e,a,b,c,d
    return chi2_sum

def Chi2OverNdf(Func,
                X,
                Y,
                Yerr,
                FitVals,
                Chi2NdfRange=[-np.inf,np.inf]):
    rangefilt = np.logical_and(X>Chi2NdfRange[0],X<Chi2NdfRange[1])
    chi2 = GetChiSquare( lambda x: Func(x,*FitVals),
                         X[rangefilt],
                         Y[rangefilt],
                         Yerr[rangefilt])
    ndf = len(X[rangefilt])-len(FitVals)
    return chi2/ndf

import scipy.stats as st


def Chi2OverNdfProb(Func,
                X,
                Y,
                Yerr,
                FitVals,
                Chi2NdfRange=[-np.inf,np.inf]):
    rangefilt = np.logical_and(X>Chi2NdfRange[0],X<Chi2NdfRange[1])
    chi2 = GetChiSquare( lambda x: Func(x,*FitVals),
                         X[rangefilt],
                         Y[rangefilt],
                         Yerr[rangefilt])
    ndf = len(X[rangefilt])-len(FitVals)
    return chi2/ndf,st.chisqprob(chi2,ndf)
    
def PlotOptions(options):
    option_dict = vars(options)
    maxlen = -1
    for _opt in option_dict.keys():
        if len(_opt)>maxlen:
            maxlen = len(_opt)
    
    OPTS = zip(option_dict.keys(),option_dict.values())
    OPTS.sort()
    print ''
    print '---------------------'
    print '------ Options ------'
    print '---------------------'
    for dd,kk in OPTS:
        dd = dd + ':'
        print '\t'+dd.ljust(maxlen+1)+'\t'+str(kk)
    print '---------------------'
    print ''


def BinsEdgesFromBinCenters(Binctrs,log=True):
    if log:
        FakMean=np.mean(Binctrs[1:]/Binctrs[:-1])
        BinLow = list(Binctrs/np.sqrt(FakMean))
        BinLow.append(BinLow[-1]*FakMean)
        return np.array(BinLow)
    else:
        FakMean=np.mean(Binctrs[1:]-Binctrs[:-1])
        BinLow = list(Binctrs - FakMean/2)
        BinLow.append(BinLow[-1]+FakMean)
        return np.array(BinLow)
    return False

def BinsCentersFromBinEdges(Binedges,log=True):
    if log:
        FakMean=np.mean(Binedges[1:]/Binedges[:-1])
        return Binedges[:-1]*np.sqrt(FakMean)
    else:
        FakMean=Binedges[1:] + Binedges[:-1]
        return FakMean/2

def ensure_path(Path):
    if not os.path.exists(Path):
        print 'Path ' + Path +' does not exist! Creating path!'
        os.makedirs(Path)
